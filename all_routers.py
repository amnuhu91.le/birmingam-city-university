from netmiko import ConnectionHandler
from Main_Router import base_router



north = {
	'device_type':'cisco_ios',
	'host':'192.168.3.22',
	'username':'cisconorth',
	'password':'routernorth'
}




center = {
	'device_type':'cisco_ios',
	'host':'192.168.2.22',
	'username':'routercity',
	'password':'ciscocity'	
}



south = {
	'device_type':'cisco_ios',
	'host':'192.168.1.22',
	'username':'ciscosouth',
	'password':'routersouth'	
}


with open('command_list.txt','r') as file:
	comm = file.read().splitlines()


routers = [north,south,center] 

for router in routers:
	net_connect = ConnectionHandler(**router)
	net_connect.send_config_set(comm)



with open('shotest_path.txt','r') as file:
	commands  = file.read().splitlines()

router_path = ConnectionHandler(**base_router)
router_path.send_config_set(commands)

with open('shotest_path.txt','r') as f:
    confs  = file.read().splitlines()

   
eig_path = ConnectionHandler(**base_router)
eig_path.send_config_set(confs)



