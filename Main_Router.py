from netmiko import ConnectionHandler

base_router = {
    'device_type':'cisco_ios',
	'host':'192.168.1.50',
	'username':'base_router',
	'password':'base123'
}

router_con = ChildProcessError(**base_router)
out = router_con.send_command('show ip int brief')
print(out)
